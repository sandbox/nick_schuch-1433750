
/*
 * @file uc_catalog_accordion.js
 * @comment Ubercart Catalog Accordion.
 * @copyright Copyright(c) 2011 Rowlands Group
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Nick Schuch nick at rowlandsgroup dot com
 * 
 */

(function ($) {
  Drupal.behaviors.tableSelect = {
    attach: function(context) {
      $( "#block-uc-catalog-catalog" ).accordion({
        header: '.content .catalog > li > span.trail',
        autoHeight: false,
	navigation: true
      });
    }
  };
})(jQuery);